# Solution 1 -
CreateAndEmptyList project
It is creating a new Linked List with collection of Integer objects.
The main thread then spawns 2 Threads -
First thread removes Integer objects < 5000
Second thread removes Integer objects > 500
Main thread waits for both the spawn threads to complete. Once the threads are complete, it verifies whether the List is empty.
Finally, it returns with a success message.

# Solution 2 -
LogArgumentsTest Project
It has declared a new Custom Annotation LogArguments which can be used to log arguments.
If any method is using annotation, it will log the arguments for the method.
If a class is using annotation, it will log the arguments for all the methods declared in the class.
