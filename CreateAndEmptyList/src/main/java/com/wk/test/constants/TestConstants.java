package com.wk.test.constants;

public class TestConstants {
	public static final int LIST_SIZE = 1000;
	public static final int MAX_NUMBER = 10000;

	public static final String ERR_LIST_NOT_EMPTY = "Not all Objects were removed from List";
	public static final String ERR_LIST_IS_EMPTY = "List is not populated";
	public static final String SUCC_MESSAGE = "List is now empty";
}
