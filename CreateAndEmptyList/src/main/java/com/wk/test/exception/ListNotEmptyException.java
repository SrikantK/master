package com.wk.test.exception;

public class ListNotEmptyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ListNotEmptyException(String message) {
		super(message);
	}

}
