package com.wk.test.exception;

public class ListIsEmptyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ListIsEmptyException(String message) {
		super(message);
	}

}
