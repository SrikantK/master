package com.wk.test;

import static com.wk.test.constants.TestConstants.ERR_LIST_IS_EMPTY;
import static com.wk.test.constants.TestConstants.ERR_LIST_NOT_EMPTY;
import static com.wk.test.constants.TestConstants.LIST_SIZE;
import static com.wk.test.constants.TestConstants.MAX_NUMBER;
import static com.wk.test.constants.TestConstants.SUCC_MESSAGE;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.wk.test.exception.ListIsEmptyException;
import com.wk.test.exception.ListNotEmptyException;
import com.wk.test.util.ExceptionUtil;

public class CreateAndEmptyList {
	private static final Logger logger = Logger.getLogger(CreateAndEmptyList.class);
	private static final Random random = new Random();

	private List<Integer> prepareList() {
		logger.debug("Preparing Integer List of size " + LIST_SIZE);
		List<Integer> integerList = new LinkedList<Integer>();
		for (int i = 0; i < LIST_SIZE; i++) {
			integerList.add(random.nextInt(MAX_NUMBER));
		}
		logger.debug("Created Integer List =>" + integerList);
		return Collections.synchronizedList(integerList);
	}

	public String doProcess() throws Exception {
		final List<Integer> list = prepareList();
		if (list == null || list.isEmpty()) {
			throw new ListIsEmptyException(ERR_LIST_IS_EMPTY);
		}

		try {
			Thread firstThread = new Thread(new RemoveObjectWorker(list, obj -> (obj < 5000)), "Thread 1");
			Thread secondThread = new Thread(new RemoveObjectWorker(list, obj -> (obj > 500)), "Thread 2");
			firstThread.start();
			secondThread.start();
			firstThread.join();
			secondThread.join();
		} catch (Exception ex) {
			ExceptionUtil.handleError(ex);
			throw ex;
		}

		if (!list.isEmpty()) {
			throw new ListNotEmptyException(ERR_LIST_NOT_EMPTY);
		}
		return SUCC_MESSAGE;
	}

	public static void main(String[] args) throws Exception {
		CreateAndEmptyList test = new CreateAndEmptyList();
		String result = test.doProcess();
		logger.debug(result);
	}
}
