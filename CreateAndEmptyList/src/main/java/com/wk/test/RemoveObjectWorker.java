package com.wk.test;

import java.util.List;
import java.util.function.Predicate;

public class RemoveObjectWorker implements Runnable {
	private final List<Integer> objectList;
	private final Predicate<Integer> predicate;

	public RemoveObjectWorker(List<Integer> objectList, Predicate<Integer> predicate) {
		this.objectList = objectList;
		this.predicate = predicate;
	}

	@Override
	public void run() {
		doWork();
	}

	private void doWork() {
		this.objectList.removeIf(this.predicate);
	}
}

class Criteria {
	private int startValue;
	private boolean isGreatherThan;

	public int getStartValue() {
		return startValue;
	}

	public void setStartValue(int startValue) {
		this.startValue = startValue;
	}

	public boolean isGreatherThan() {
		return isGreatherThan;
	}

	public void setGreatherThan(boolean isGreatherThan) {
		this.isGreatherThan = isGreatherThan;
	}

}
