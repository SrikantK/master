package com.wk.test.util;

import org.apache.log4j.Logger;

public class ExceptionUtil {
	private static final Logger logger = Logger.getLogger(ExceptionUtil.class);

	public static void handleError(Throwable ex) {
		logger.error(ex.getMessage(), ex);
	}
}
