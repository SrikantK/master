package com.wk.test;

import static com.wk.test.constants.TestConstants.SUCC_MESSAGE;
import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestCreateAndEmptyList {
	private CreateAndEmptyList testObj;

	@Before
	public void setUp() throws Exception {
		testObj = new CreateAndEmptyList();
	}

	@Test
	public void doProcess_NormalInput_ShouldReturnSuccessMessage() {
		try {
			String result = testObj.doProcess();
			Assert.assertEquals("Result message not expected", SUCC_MESSAGE, result);
		} catch (Exception e) {
			fail("Got error ->" + e.getMessage());
		}

	}

}
