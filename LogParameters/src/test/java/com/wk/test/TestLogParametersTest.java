package com.wk.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.wk.test.bean.TestComponent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LogParamApp.class)
public class TestLogParametersTest {

	@Autowired
	private TestComponent testComponent;

	@Test
	public void invokeTestComponent_PassedTestComponent_ShouldLogArguments() {
		LogParamApp.invokeTestComponent(testComponent);
	}

}
