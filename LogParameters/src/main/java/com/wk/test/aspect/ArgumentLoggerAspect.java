package com.wk.test.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ArgumentLoggerAspect {
	private static final Logger logger = LoggerFactory.getLogger(ArgumentLoggerAspect.class);

	@Before("@annotation(com.wk.test.annotation.LogArguments) || within(@com.wk.test.annotation.LogArguments *)")
	public void logArguments(JoinPoint joinPoint) {
		StringBuilder builder = new StringBuilder();
		Object[] arguments = joinPoint.getArgs();
		String className = joinPoint.getSignature().getDeclaringTypeName();
		String methodName = joinPoint.getSignature().getName();
		builder.append("\n********").append(className).append(".").append(methodName).append("***********\n");

		if (arguments.length == 0) {
			builder.append("No args method");
		} else {
			for (int i = 0; i < arguments.length; i++) {
				builder.append("[Argument " + i + "]=>" + arguments[i]).append("\n");
			}
		}
		logger.debug(builder.toString());
	}
}
