package com.wk.test.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.wk.test.annotation.LogArguments;

@Component
@LogArguments
public class SecondComponent {
	private static final Logger logger = LoggerFactory.getLogger(SecondComponent.class);

	public void testMethod(Integer a, Integer b) {
		logger.debug("Test Method Called");
	}

}
