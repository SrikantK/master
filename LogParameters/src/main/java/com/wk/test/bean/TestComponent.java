package com.wk.test.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.wk.test.annotation.LogArguments;

@Component
public class TestComponent {
	private static final Logger logger = LoggerFactory.getLogger(TestComponent.class);

	@LogArguments
	public void testMethod(Integer a, Integer b) {
		logger.debug("Test Method Called");
	}

	public void secondMethod(Integer a, Integer b) {
		logger.debug("Second Method Called");
	}

	@LogArguments
	public void thirdMethod() {
		logger.debug("Third method called");
	}
}
