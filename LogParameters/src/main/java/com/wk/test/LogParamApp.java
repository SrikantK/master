package com.wk.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.wk.test.bean.SecondComponent;
import com.wk.test.bean.TestComponent;

@SpringBootApplication
public class LogParamApp {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(LogParamApp.class, args);
		TestComponent firstComp = context.getBean(TestComponent.class);
		invokeTestComponent(firstComp);

		SecondComponent secondComp = context.getBean(SecondComponent.class);
		invokeSecondComponent(secondComp);
	}

	public static void invokeTestComponent(TestComponent comp) {
		comp.testMethod(1, 2);
		comp.secondMethod(3, 4);
		comp.thirdMethod();
	}

	public static void invokeSecondComponent(SecondComponent comp) {
		comp.testMethod(1, 2);
	}

}
